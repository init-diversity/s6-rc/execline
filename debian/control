Source: execline
Section: init-diversity
Priority: optional
Maintainer: ProwlerGr <Prowler73@gmail.com>
Rules-Requires-Root: no
Build-Depends:
 debhelper-compat (= 13),
Build-Depends-Arch:
 skalibs-dev (>= 2.14.0.0),
Standards-Version: 4.6.2
Homepage: https://skarnet.org/software/execline/
Vcs-Git: https://salsa.debian.org/zhsj/execline.git
Vcs-Browser: https://salsa.debian.org/zhsj/execline

Package: execline
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Suggests:
 execline-doc,
Description: small and non-interactive scripting language
 Execline is a (non-interactive) scripting language, like sh; but its
 syntax is quite different from a traditional shell syntax. The execlineb
 program is meant to be used as an interpreter for a text file; the other
 commands are essentially useful inside an execlineb script.
 .
 Execline is as powerful as a shell: it features conditional loops,
 getopt-style option handling, filename globbing, and more. Meanwhile, its
 syntax is far more logic and predictable than the shell's syntax, and has
 no security issues.
 .
 Documentation is provided in execline-doc package.
 .
 Binaries are installed in /usr/lib/execline/bin. Use the /usr/bin/execlineb
 wrapper or add /usr/lib/execline/bin to PATH.

Package: execline-doc
Architecture: all
Section: init-diversity
Multi-Arch: foreign
Depends:
 ${misc:Depends},
Description: small and non-interactive scripting language (documentation)
 Execline is a (non-interactive) scripting language, like sh; but its
 syntax is quite different from a traditional shell syntax. The execlineb
 program is meant to be used as an interpreter for a text file; the other
 commands are essentially useful inside an execlineb script.
 .
 Execline is as powerful as a shell: it features conditional loops,
 getopt-style option handling, filename globbing, and more. Meanwhile, its
 syntax is far more logic and predictable than the shell's syntax, and has
 no security issues.
 .
 This package contains documentation files.

Package: libexecline-dev
Architecture: any
Section: init-diversity
Multi-Arch: same
Depends:
 libexecline2.9 (= ${binary:Version}),
 ${misc:Depends},
Description: small and non-interactive scripting language (development files)
 Execline is a (non-interactive) scripting language, like sh; but its
 syntax is quite different from a traditional shell syntax. The execlineb
 program is meant to be used as an interpreter for a text file; the other
 commands are essentially useful inside an execlineb script.
 .
 Execline is as powerful as a shell: it features conditional loops,
 getopt-style option handling, filename globbing, and more. Meanwhile, its
 syntax is far more logic and predictable than the shell's syntax, and has
 no security issues.
 .
 This package contains static and header files.

Package: libexecline2.9
Architecture: any
Section: libs
Multi-Arch: same
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: small and non-interactive scripting language (shared library)
 Execline is a (non-interactive) scripting language, like sh; but its
 syntax is quite different from a traditional shell syntax. The execlineb
 program is meant to be used as an interpreter for a text file; the other
 commands are essentially useful inside an execlineb script.
 .
 Execline is as powerful as a shell: it features conditional loops,
 getopt-style option handling, filename globbing, and more. Meanwhile, its
 syntax is far more logic and predictable than the shell's syntax, and has
 no security issues.
 .
 This package contains shared libraries.
