# execline - an interpreter-less scripting language
-------------------------------------------------

 execline is a scripting language unlike any other in that
it has no resident interpreter. It reads a script, turns it
into a single command line, and executes into that command
line; control is performed by executables run inside the
command line itself.

 It is especially suited to very small and simple scripts
for which a shell is overpowered.

 See https://skarnet.org/software/execline/ for details.


## Installation
  ------------

 See the INSTALL file.

## Recommended build instructions

The recommended method to build this package directly from git.
```
gbp clone https://gitlab.com/init-diversity/s6-rc/execline.git && 
cd execline && 
gbp buildpackage -uc -us
```

The following should get you all the software required to build using this method:

```
sudo apt install git-buildpackage skalibs-dev
```

## Customization

You can customize paths via flags given to configure. See `./configure --help` for a list of all available configure options.

These flags will need to be added to the `debian/rules` file.



* Contact information
  -------------------

 Laurent Bercot <ska-skaware at skarnet.org>

 Please use the <skaware at list.skarnet.org> mailing-list for
questions about execline.
